We switched to https://github.com/Wiolarz/Console_Dungeon





# Console Dungeon

A text based game, created by the students of PJATK. The project was created with the idea to practise java by doing something together.

## Getting started

Read project wiki. In order to contribute you will need to create gitlab account, and send request for access to this project.

Project is made in java 16.
The IDE (integrated development environment) we use is an IntellJi.


## License
Its an open source projects, Apache licence.

## Project status
We just started 24.11.2021

## Authors and acknowledgment

Wiolarz, Paul Berg
